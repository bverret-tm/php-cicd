# Bitbucket Pipelines

Explication de l'expérimentation d'un pipeline de déploiement avec Bitbucket Pipelines.
L'utilisation de PHP est uniquement pour la démonstration,
les concepts peuvent être appliqués à n'importe quel langage.

## Fichier `bitbucket-pipelines.yml`

Contrairement à `.gitlab-ci.yml`, `bitbucket-pipelines.yml` ne peut pas être fragmenté.

### Fonction prédéfinie

L'utilisation des ancres YAML permet de créer des segments qui pourront être réutilisés au travers des étapes du pipeline.

La fonction prédéfinie `registry_login` sert à se connecter sur le registre de DockerHub. Contrairement à GitLab, Bitbucket ne possède pas son propre registre de conteneur.

```sh
  function registry_login() {
    if [[ -n "$DOCKER_USERNAME" ]]; then
        echo "Logging to Docker Hub with credentials..."
        echo $DOCKER_PASSWORD | docker login -u "$DOCKER_USERNAME" --password-stdin docker.io
        echo ""
    fi
  }
```

Bien qu'offrant beaucoup de fonctionnalité, la fonction `build_docker` ne peut pas être utilisée à son plein potentiel comme avec GitLab CI. La raison principale est l'incapacité d'utiliser des variables comme nom d'image dans les étapes du pipeline, il n'est donc pas possible de faire varier l'image utilisée dynamiquement, on doit plutôt créer des étapes pour chaque cas d'utilisation.

```sh
  function build_docker() {
    dockerfile="Dockerfile"
    image_name="$DOCKER_USERNAME/$BITBUCKET_REPO_SLUG"
    echo "Set registry image name to build..."
    while getopts ":i:d:a:" opt; do
      case $opt in
        i) image_name="$OPTARG"
        ;;
        d) dockerfile="$OPTARG"
        ;;
        a) args="$OPTARG"
        ;;
      esac
    done
    echo "Image name set to $image_name"
    echo "Dockerfile set to $dockerfile"
    echo ""
    echo "Pull image from registry to by used as cache..."
    docker pull --quiet $image_name:latest || true
    echo ""
    if [ -n "$args" ]; then
      echo "Build image with args $args from pulled image cache and create `latest` and `commit_sha` tags..."
      docker build --quiet --cache-from $image_name:latest --build-arg $args --tag $image_name:$BITBUCKET_COMMIT --tag $image_name:latest --file "docker/$dockerfile" "docker/"
    else
      echo "Build image from pulled image cache and create `latest` and `commit_sha` tags..."
      docker build --quiet --cache-from $image_name:latest --tag $image_name:$BITBUCKET_COMMIT --tag $image_name:latest --file "docker/$dockerfile" "docker/"
    fi
    echo ""
    echo "Push the tagged Docker images to the container registry.."
    docker push $image_name:$BITBUCKET_COMMIT
    docker push $image_name:latest
    echo ""
  }
```

Contrairement à GitLab CI, les variables d'environnement utilisé dans Bitbucket doivent obligatoirement être créées dans le projet et ne peuvent être modifiées à même le pipeline. La seule façon de passer des informations entre les étapes est d'utiliser les __artifacts__. La fonction `set_environment_variable` sert à créer les variables d'environnement généré par une étape précédente et exporté en __artifact__. Plus de détail dans l'étape [**Variables**](###variables).

```sh
  function set_environment_variable() {
    echo "Set environment variable..."
    cat set_env.sh
    source set_env.sh
    echo ""
  }
```

### Variables

Puisque le principe de variable pouvant être créé, modifié et partagé entre les diverses étapes n'existe pas avec Bitbucket, la façon détournée de le faire est de créer un fichier `set_env.sh`, d'y ajouter la définition des variables désirée et de l'exporter en **artifacts**. Le fichier pourra donc être appelé au début des autres étapes pour configurer les variables d'environnement.

Si lors d'une étape subséquente nous désirons modifier une variable, nous devons de nouveau déclarer l'artifacts et ajouter à la fin la nouvelle déclaration de variable. La variable sera donc écrasée par la nouvelle lors de l'appel du fichier.

```yml
    - step:
        name: Variables
        script:
          - *auto_devops
          - echo "export PHP_IMAGE='$DOCKER_USERNAME/$BITBUCKET_REPO_SLUG'" >> set_env.sh
          - echo "export PHP_TEST_IMAGE='$DOCKER_USERNAME/$BITBUCKET_REPO_SLUG-test'" >> set_env.sh
          - if [[ "$BITBUCKET_BRANCH" == "master" ]]; then
          -   echo "export STAGE='staging'" >> set_env.sh
          - elif [[ "$BITBUCKET_BRANCH" == "production" ]]; then
          -   echo "export STAGE='production'" >> set_env.sh
          - else
          -   echo "export STAGE='no-stage'" >> set_env.sh
          - fi
        artifacts:
          - set_env.sh
```

### Build

Pour le build, si nous désirons générer des images dockers nous devons utiliser le service `docker`. Nous pouvons aussi utiliser la `caches` pour accélérer la réutilisation de l'image créée dans des étapes subséquentes.

```yml
        services:
          - docker
        caches:
          - docker
```

Bitbucket possède la clef `condition`. Cette clef semble relativement nouvelle et ne possède que l'option `changesets`. Elle permet de définir une condition pour démarrer l'étape. Si cette condition n'est pas remplie, elle sera sautée à l'option suivante.

Dans cet exemple, on inclut les Paths des fichiers/dossiers qui doivent avoir été modifiés pour démarrer l'étape.

```yml
        condition:
          changesets:
            includePaths:
              - "docker/Dockerfile"
```

Pour pouvoir gérer les situations d'échec, nous utilisons `after-script`. Ces scripts sont exécutés, à la fin peu importe si la section script est un succès ou non. Pour savoir si le `script` était un succès, on peut utiliser la variable prédéfinie de Bitbucket `$BITBUCKET_EXIT_CODE` pour connaître le code de sortie, la valeur 0 signifie un succès, sinon il s'agit d'un échec. On peut donc exécuter un script de gestion d'échec.

```yml
        after-script:
          - if [[ $BITBUCKET_EXIT_CODE == 0 ]]; then
          -   echo "Build successfully work"
          - else
          -   chmod +x scripts/on_build_failure.sh && sh scripts/on_build_failure.sh
          - fi
```

### Test

Pour les Tests, voir le cas d'exemple de GitLab CI [PHP-CICD](https://gitlab.com/benoit.verret.tm/php-cicd/-/blob/master/README.md) ainsi que l'étape du build pour les différentes sections du code.

### Deploy

Je vais aborder uniquement le `set +e`, pour le reste, voir le cas d'exemple de GitLab CI [PHP-CICD](https://gitlab.com/benoit.verret.tm/php-cicd/-/blob/master/README.md).

GitLab CI offre la possibilité de permettre l'échec d'une étape sans arrêter le pipeline, pour reproduire le tout avec Bitbucket nous devons jouer avec les mécaniques qu'ils utilisent.

L'exécution de `set +e` désactivera l'arrêt en cas d'échec pour tout ce qui suit et uniquement à l'intérieur de l'étape où il est utilisé. Il est possible de le réactiver avec `set -e`, nous pouvons donc entourer une section de code où on permettrait l'échec.

```yml
    - step:
        name: Deploy:chatops
        script:
          - set +e # Allow to fail
          - *auto_devops
          - set_environment_variable
          - echo "Execute some chatbot to ask manual deploy"
```

### Les Pipes

GitLab permet d'importer des scripts CI depuis d'autres repos, ce qui rend réutilisable le code. Bitbucket n'offre pas autant de malléabilité, mais offre **Pipes**. En résumé, c'est un repository qui génère un Dockerfile et possède aussi un fichier `entrypoint` qui exécutera des mécaniques désirées. L'utilité est que Bitbucket permet de passer des arguments au fichier entrypoint. On pourrait donc imaginer un Pipe servant à invalider la cache CloudFormation en lui passant en argument région, compte, et credentials AWS. Le pipe est exécuté localement à même le Pipeline. Autre cas est pour le déploiement, on peut imaginer une mécanique qui exporte un site statique vers un Bucket S3. On pourrait aussi avoir une mécanique de notification vers Slack. Bitbucket offre aussi une large librairie de Pipes prédéfinie.

Pour plus d'information sur les [Pipes](https://support.atlassian.com/bitbucket-cloud/docs/write-a-pipe-for-bitbucket-pipelines/).
